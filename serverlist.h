#ifndef SERVERLIST_H
#define SERVERLIST_H

#include <QObject>
#include <QVector>
#include <QTimer>
#include <QPoint>
#include <QAction>
#include <QMenu>
#include <QTreeWidgetItem>

#include "serveraddress.h"
#include "servermanager.h"

namespace Ui {
class Launcher;
}

class ServerTreeWidget;
class ServerTreeWidgetItem;

class ServerList : public QObject
{
    Q_OBJECT

public:
    explicit ServerList(Ui::Launcher *ui, QObject *parent = nullptr);
    ~ServerList();

    void setupTree(ServerTreeWidget *tree) { m_pServerTree = tree; }
    void refreshOne(const ServerAddress& address);
    void refreshAll(ServerTreeWidget* serverList);

protected slots:
    virtual void onTimeout();
    virtual void onServerInfo(PacketInfo packet);
    virtual void onServerDetails(PacketDetails packet);
    virtual void onServerSelectionChanged();
    virtual void onServerDoubleClicked(QTreeWidgetItem *pItem, int id);
    virtual void onServerKeyEnter(QTreeWidgetItem *pItem);
    virtual void onServerTabChanged(int id);
    virtual void onContextMenu(const QPoint &point);

protected:
    void join(ServerAddress address, Version version);

    ServerManager m_Manager;
    QThread m_Thread;
    QTimer *m_pTimer;
    Ui::Launcher *pUi;
    QString m_World;
    QMenu m_ContextMenu;

private:
    ServerTreeWidget *m_pServerTree = nullptr;
};

#endif // SERVERLIST_H
