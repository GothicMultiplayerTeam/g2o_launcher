#ifndef UPDATESCRIPT_H
#define UPDATESCRIPT_H

#include <QString>

class UpdateScript
{
public:
    UpdateScript(const QString& update_uuid);

    void execute();

private:
    QString main_dir;
    QString multiplayer_dir;
    QString update_dir;
    QString update_uuid_file;

    void writeBatchScript(const QString& update_uuid);
    void startConsole();
};

#endif // UPDATESCRIPT_H
