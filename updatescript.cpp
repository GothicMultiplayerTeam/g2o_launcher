#include <QProcess>
#include <QDir>
#include <QFileInfo>
#include <QTextStream>
#include <QTextCodec>
#include <QVector>
#include <QCoreApplication>
#include <Windows.h>

#include "updatescript.h"
#include "config.h"

static int getSystemCodePage()
{
    LCID lcid = GetUserDefaultLCID();
    if (lcid == 0)
        return 1250;

    char codePage[256];
    int size = GetLocaleInfoA(lcid, LOCALE_IDEFAULTANSICODEPAGE, codePage, sizeof(codePage));

    if (size == 0)
        return 1250;

    try {
        return std::stoi(codePage);
    }
    catch (...) {}

    return 1250;
}

UpdateScript::UpdateScript(const QString& update_uuid)
{
    main_dir = Config::get().getGameDir().replace('/', '\\');
    multiplayer_dir = main_dir + "\\Multiplayer";
    update_dir = multiplayer_dir + "\\update";
    update_uuid_file = multiplayer_dir + "\\update_uuid";

    writeBatchScript(update_uuid);
}

void UpdateScript::writeBatchScript(const QString& update_uuid)
{
    QFile script(multiplayer_dir + "/update.bat");
    if (!script.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream stream(&script);
    stream.setCodec(QTextCodec::codecForLocale());

    QString g2o_launcher_path = QCoreApplication::applicationDirPath().replace('/', '\\');

    stream << "@echo off\n\n";

    stream << "title g2o updater\n"
           << "chcp " << getSystemCodePage() << " > NUL\n"
           << "taskkill /IM \"G2O_Launcher.exe\" >NUL 2>&1\n\n"
           << "echo DO NOT CLOSE THIS WINDOW! DOING THAT MIGHT BREAK G2O CLIENT!!!\n\n";

    stream << "echo killing launcher process...\n"
           << "taskkill /IM \"G2O_Launcher.exe\" >NUL 2>&1\n\n";

    if (QFile::exists(update_dir + "\\milestone.exe"))
    {
        stream << "echo uninstalling old client...\n"
               << QString("\"%1\\G2O_Uninstall.exe\" /S 2> NUL\n").arg(main_dir)
               << QString("\"%1\\milestone.exe\" /S /D=%2\n").arg(update_dir, main_dir)
               << QString("del \"%1\\milestone.exe\"\n").arg(update_dir)
               << "\n";
    }

    stream << "echo patching...\n"
           << QString("xcopy \"%1\" \"%2\" /Q /E /I /Y > NUL\n").arg(update_dir, main_dir)
           << QString("rmdir \"%1\" /S /Q\n").arg(update_dir)
           << "\n";

    stream << "echo setting update_uuid...\n"
           << QString("echo %1> \"%2\"\n\n").arg(update_uuid, update_uuid_file);

    stream << "echo trying to start the launcher...\n"
           << QString("if exist \"%1\\G2O_Launcher.exe\" (\n").arg(g2o_launcher_path)
           << QString("	start \"\" /B \"%1\\G2O_Launcher.exe\"\n").arg(g2o_launcher_path)
           << ") else (\n"
           << "	echo launcher not found! run it manually.\n"
           << ")\n\n";

    stream << "echo update completed!\n"
           << "timeout /T 1 /nobreak > NUL\n"
           << "goto 2>nul & del \"%~f0\" & exit";

    stream.flush();
    script.close();
}

void UpdateScript::startConsole()
{
    PROCESS_INFORMATION processInfo = {};

    STARTUPINFOA startupInfo = {};
    startupInfo.cb = sizeof(startupInfo);
    startupInfo.dwFlags = STARTF_USESHOWWINDOW;
    startupInfo.wShowWindow = SW_SHOW;

    LPCSTR cmdLine = "cmd.exe /K ..\\update.bat";
    if (CreateProcessA(nullptr, const_cast<LPSTR>(cmdLine), nullptr, nullptr, false, CREATE_NEW_CONSOLE, nullptr, nullptr, &startupInfo, &processInfo))
    {
        CloseHandle(processInfo.hProcess);
        CloseHandle(processInfo.hThread);
    }
}

void UpdateScript::execute()
{
    startConsole();
    QCoreApplication::quit();
}
