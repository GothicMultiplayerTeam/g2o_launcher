#include <QPixmap>
#include <QPalette>
#include <QDir>
#include <QMessageBox>
#include <QsseFileLoader.h>

#include "launcher.h"
#include "g2o.h"
#include "config.h"
#include "ui_launcher.h"

Launcher::Launcher(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Launcher),
    m_NetList(ui),
    m_FavList(ui)
{
    ui->setupUi(this);
    QDir::setCurrent(qApp->applicationDirPath());

    setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMinimizeButtonHint);

    // Register as meta type
    qRegisterMetaType<ServerAddress>("ServerAddress");
    qRegisterMetaType<PacketPing>("PacketPing");
    qRegisterMetaType<PacketInfo>("PacketInfo");
    qRegisterMetaType<PacketDetails>("PacketDetails");

    loadAssets();
    loadConfig();

    m_NetList.init();
    m_FavList.init();

    m_NetList.refreshAll();

#ifdef QT_DEBUG
    m_Watcher.addPath(QString(PROJECT_DIR) + "/resources/style.css");
#endif

    // Check for updates
    m_Updater.check();
}

Launcher::~Launcher()
{
    Config::get().setNickname(ui->editNickname->text());
    delete ui;
}

void Launcher::init()
{
    ui->labVersion->setText(G2O::versionText());

    // Connections
    connect(ui->btnRefresh, &QPushButton::released, this, &Launcher::onRefresh);
    connect(&m_NetList, &InternetList::favoriteAdded, &m_FavList, &FavoriteList::onFavoriteAdded);

#ifdef QT_DEBUG
    connect(&m_Watcher, &QFileSystemWatcher::fileChanged, this, &Launcher::onStyleSheetReload);
#endif
}

void Launcher::loadConfig()
{
    ui->editNickname->setText(Config::get().getNickname());
}

void Launcher::loadAssets()
{
    QPixmap bg(":/resources/background.jpg");
    bg = bg.scaled(size(), Qt::IgnoreAspectRatio);

    QPalette palette;
    palette.setBrush(QPalette::Background, bg);
    setPalette(palette);

    using namespace ibkrj::xamgu;

    QsseFileLoader loader(":/resources/");
    qApp->setStyleSheet(loader.loadStyleSheet("style.css"));

    update();
}

void Launcher::onRefresh()
{
    switch (ui->tabServers->currentIndex())
    {
    case 0: // Internet
        m_NetList.refreshAll();
        break;

    case 1: // Favorite
        m_FavList.refreshAll();
        break;
    }
}

#ifdef QT_DEBUG
void Launcher::onStyleSheetReload()
{
    using namespace ibkrj::xamgu;

    QsseFileLoader loader(QString(PROJECT_DIR) + "/resources/");
    qApp->setStyleSheet(loader.loadStyleSheet("style.css"));
}
#endif
