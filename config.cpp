#include "config.h"

#include <QFile>
#include <QTextStream>
#include <QDir>
#include <QCoreApplication>

Config::Config() :
    m_userRegistry("HKEY_CURRENT_USER\\Software\\G2O", QSettings::NativeFormat)
{

}

Config& Config::get()
{
    static Config instance;
    return instance;
}

QString Config::getGameDir()
{
    QDir launcher_dir(QCoreApplication::applicationDirPath());

    launcher_dir.cdUp();
    launcher_dir.cdUp();

    return launcher_dir.absolutePath();
}

QString Config::getUpdateUUID()
{
    QDir update_uuid_dir(QCoreApplication::applicationDirPath());
    update_uuid_dir.cdUp();

    QFile file(update_uuid_dir.filePath("update_uuid"));
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return "";

    QTextStream text_stream(&file);
    QString uuid = text_stream.readLine();

    file.close();

    return uuid;
}

QString Config::getNickname()
{
    return m_userRegistry.value("nickname", "Nickname").toString();
}

void Config::setNickname(const QString& nickname)
{
    // Convert unicode to the ASCII
    m_userRegistry.setValue("nickname", nickname.toStdString().c_str());
}
