#ifndef AUTOUPDATER_H
#define AUTOUPDATER_H

#include <QWidget>
#include <QFile>
#include <QBuffer>
#include <QTimer>

#include "httpmanager.h"
#include "downloadmanager.h"

namespace Ui {
class AutoUpdater;
}

class AutoUpdater : public QWidget
{
    Q_OBJECT

public:
    explicit AutoUpdater(QWidget *parent = 0);
    ~AutoUpdater();

public slots:
    void check();

private slots:
    void onRequestFinished(QUrl url, QJsonDocument doc);
    void onDownloadFinished(QUrl url, QByteArray data);
    void onDownloadProgress(qint64 current, qint64 max);
    void onDownloadCompleted();

private:
    void versionOutdated(const QJsonArray& patches);
    void enqueueFilesToDownload(const QJsonArray& patches);

    Ui::AutoUpdater *ui;

    QTimer *m_pUpdateTimer;
    HttpManager m_Http;
    DownloadManager m_Download;

    QString m_MilestoneURL;
    QString m_UpdateUUID;

    qint64 m_CurrentProgress = 0;
    qint64 m_MaxProgress = 0;
};

#endif // AUTOUPDATER_H
