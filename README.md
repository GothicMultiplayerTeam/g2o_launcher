# Gothic 2 Online - Launcher
Official launcher for modification Gothic 2 Online.

## Demo
![gif showing how launcher looks like](launcher.gif)

## Technology
Launcher is build with Qt 5.15.2 and uses these dependencies:
  - [qt-5.15.16-msvc2019-Win32](dependencies/qt-5.15.16-msvc2019-Win32.zip) - custom precompiled qt toolset that provides newer version of **Qt** + **OpenSSL 3.x** support.
  - [zlib 1.2.11](https://zlib.net/) - popular compression library
  - [QuaZip 0.7.3](http://quazip.sourceforge.net/) - a simple C++ Qt wrapper over Gilles Vollant's ZIP/UNZIP package that can be used to access ZIP archives
  - [OpenSSL 3.4.0](https://slproweb.com/products/Win32OpenSSL.html) - Secure Sockets Layer, required for `https://` protocol support.
  - [xamgu-QSS-loader](https://github.com/mrkkrj/xamgu-QSS-loader) - custom qt style sheets loader that allows define variables & load other qss files

## Building
Make sure to install the [git lfs](https://git-lfs.com/) before you fetch the source code.

In order to build the launcher you'll need to download precompiled [qt toolset](dependencies/qt-5.15.16-msvc2019-Win32.zip) and unpack it to your Qt folder, e.g: `C:/Qt/VERSION`.  
You'll also need to add a **toolset** manually in **Qt Creator** to able to build the project (go to `tools->external->configure` and add the toolset manually).

## License
The code is licensed under **GNU GENERAL PUBLIC LICENSE**.