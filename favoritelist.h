#ifndef FAVORITELIST_H
#define FAVORITELIST_H

#include "serverlist.h"

class FavoriteList : public ServerList
{
    Q_OBJECT

public:
    FavoriteList(Ui::Launcher *ui);
    void init();
    void refreshAll();

public slots:
    void onFavoriteAdded(const ServerAddress& address);

private slots:
    void onConnect();
    void onTimeout() override;
    void onAdd();
    void onEdit();
    void onRemove();

private:
    QSet<QString> m_addresses;

    void setupMenu();
    void load();
    void save();
};

#endif // FAVORITELIST_H
