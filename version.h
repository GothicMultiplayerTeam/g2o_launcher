#ifndef VERSION_H
#define VERSION_H

#include <QStringList>

struct Version
{
    int major = 0;
    int minor = 0;
    int patch = 0;
    int build = 0;

    static Version fromStringList(const QStringList& parts)
    {
        Version version;

        if (parts.size() >= 1) version.major = parts[0].toInt();
        if (parts.size() >= 2) version.minor = parts[1].toInt();
        if (parts.size() >= 3) version.patch = parts[2].toInt();
        if (parts.size() >= 4) version.build = parts[3].toInt();

        return version;
    }

    bool operator<(const Version& other) const
    {
        if (major != other.major) return major < other.major;
        if (minor != other.minor) return minor < other.minor;
        if (patch != other.patch) return patch < other.patch;
        return build < other.build;
    }

    bool operator>(const Version& other) const
    {
        return other < *this;
    }

    bool operator==(const Version& other) const
    {
        return major == other.major
            && minor == other.minor
            && patch == other.patch
            && build == other.build;
    }

    bool operator<=(const Version& other) const
    {
        return !(other < *this);
    }

    bool operator>=(const Version& other) const
    {
        return !(*this < other);
    }

    bool operator!=(const Version& other) const
    {
        return !(*this == other);
    }
};

#endif // VERSION_H
