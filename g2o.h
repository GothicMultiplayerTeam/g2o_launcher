#ifndef G2O_H
#define G2O_H

#include "version.h"
#include "serveraddress.h"

#define PROXY "G2O_Proxy.dll"

namespace G2O {
    Version version();
    QString versionText();
    void run(ServerAddress address, QString nickname);
}

#endif // G2O_H
