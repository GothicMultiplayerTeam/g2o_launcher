#include "httpmanager.h"
#include <QMessageBox>

HttpManager::HttpManager(QObject *parent) : QObject(parent)
{
    connect(&m_Manager, &QNetworkAccessManager::finished, this, &HttpManager::onRequestFinished);
}

void HttpManager::post(QUrl url, QJsonDocument doc)
{
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    m_Manager.post(request, doc.toJson());
}

void HttpManager::get(QUrl url)
{
    QNetworkRequest request(url);
    m_Manager.get(request);
}

void HttpManager::onRequestFinished(QNetworkReply *pReply)
{
    QJsonDocument response = QJsonDocument::fromJson(pReply->readAll());

    if (pReply->error() != QNetworkReply::NetworkError::NoError)
    {
        QMessageBox::critical(nullptr, "Request error", pReply->errorString());
    }
    else
    {
        emit finished(pReply->url(), response);
    }

    pReply->deleteLater();
}
