#include <QMessageBox>

#include "autoupdater.h"
#include "config.h"
#include "ui_autoupdater.h"
#include "updatepack.h"
#include "updatescript.h"
#include "g2o.h"

AutoUpdater::AutoUpdater(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AutoUpdater)
{
    ui->setupUi(this);

    m_pUpdateTimer = new QTimer(this);
    m_pUpdateTimer->setInterval(100 * 120); // Check update every 2 minutes
    m_pUpdateTimer->setSingleShot(true);

    connect(m_pUpdateTimer, &QTimer::timeout, this, &AutoUpdater::check);
    connect(&m_Http, &HttpManager::finished, this, &AutoUpdater::onRequestFinished);
    connect(&m_Download, &DownloadManager::finished, this, &AutoUpdater::onDownloadFinished);
    connect(&m_Download, &DownloadManager::progress, this, &AutoUpdater::onDownloadProgress);
    connect(&m_Download, &DownloadManager::completed, this, &AutoUpdater::onDownloadCompleted);
}

AutoUpdater::~AutoUpdater()
{
    delete ui;
}

void AutoUpdater::check()
{
    QString update_uuid = Config::get().getUpdateUUID();
    if (update_uuid.length())
        update_uuid += "/";

    m_Http.get(QUrl("https://api.gothic-online.com/v2/version/updates/" + update_uuid + "patches/"));
}

void AutoUpdater::versionOutdated(const QJsonArray& patches)
{
    const QJsonValue& latest_patch = patches[0].toObject();
    QString updateName = latest_patch["name"].toString();

    QString msg;

    if (updateName == G2O::versionText())
        msg += "New patch available for current version";
    else
        msg += "New update available: <b>" + updateName + "</b>";

    msg += "<br>Would you like to update?";

    QMessageBox::StandardButton reply = QMessageBox::question(this, "Updater", msg, QMessageBox::Yes | QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        m_UpdateUUID = latest_patch["uuid"].toString();

        enqueueFilesToDownload(patches);
        UpdatePack::createUpdateDir();

        m_Download.download();
        m_pUpdateTimer->stop();

        show();
    }
}

void AutoUpdater::enqueueFilesToDownload(const QJsonArray& patches)
{
    for (int i = patches.size() - 1; i >= 0; --i) {
        QJsonObject patch = patches[i].toObject();
        QString type = patch["type"].toString();

        QJsonObject package = patch["package"].toObject();
        QString download_url = package["download_url"].toString();
        QString content_type = package["content_type"].toString();
        int size = package["size"].toInt();

        // skip invalid milestone
        if (type == "milestone" && content_type != "application/x-msdos-program")
            continue;

        // skip invalid patch
        if (type == "patch" && content_type != "application/zip")
            continue;

        if (type == "milestone")
            m_MilestoneURL = download_url;

        m_Download.enqueueDownload(download_url);
        m_MaxProgress += size;
    }
}

void AutoUpdater::onRequestFinished(QUrl url, QJsonDocument doc)
{
    QJsonArray patches = doc.array();

    if (!patches.empty())
        versionOutdated(patches);
    else
        m_pUpdateTimer->start();
}

void AutoUpdater::onDownloadFinished(QUrl url, QByteArray data)
{
    UpdatePack pack(url == m_MilestoneURL, data);
    pack.extract();
}

void AutoUpdater::onDownloadProgress(qint64 current, qint64 max)
{
    Q_UNUSED(max);

    ui->progBar->setValue((m_CurrentProgress + current) * 100 / m_MaxProgress);

    if (current == max)
        m_CurrentProgress += max;
}

void AutoUpdater::onDownloadCompleted()
{
    hide();

    UpdateScript script(m_UpdateUUID);
    script.execute();
}
