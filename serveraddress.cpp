#include <QStringList>

#include "serveraddress.h"

ServerAddress ServerAddress::parse(QString ipAddr)
{
    QStringList data = ipAddr.split(':');
    if (data.size() != 2) return ServerAddress();

    bool converted = false;

    ServerAddress address { data.at(0), data.at(1).toUShort(&converted) };
    return converted ? address : ServerAddress();
}

bool ServerAddress::isUnassigned() const
{
    return IP.isEmpty();
}

QString ServerAddress::toString() const
{
    if (isUnassigned())
        return "";

    return IP + ":" + QString::number(Port);
}
