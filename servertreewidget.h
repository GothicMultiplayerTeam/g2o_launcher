#ifndef SERVERTREEWIDGET_H
#define SERVERTREEWIDGET_H

#include <QTreeWidget>
#include <QVector>
#include <QThread>

#include "serveraddress.h"
#include "servermanager.h"

class ServerTreeWidgetItem;

class ServerTreeWidget : public QTreeWidget
{
    Q_OBJECT

public:
    explicit ServerTreeWidget(QWidget *parent = 0);
    void init();
    void insert(const PacketInfo& packet);
    void update(const PacketInfo& packet, ServerTreeWidgetItem* row = nullptr);

signals:
    void enterKeyPressed(QTreeWidgetItem* item);
    void insertKeyPressed();
    void deleteKeyPressed();

private:
    ServerTreeWidgetItem* findRow(const ServerAddress& address);
    void keyPressEvent(QKeyEvent *event) override;

};

#endif // SERVERTREEWIDGET_H
